package cz.filek.aos.entity;

import cz.filek.aos.exceptions.ImageDataException;
import cz.filek.aos.image.IImageProcessor;
import cz.filek.aos.image.ImageProcessor;

import java.awt.image.BufferedImage;

/**
 * Entity for storing generated projections of image
 */
public class ImageData
{
    /**
     * Image array
     */
    private int[][] imageArray;
    /**
     * Summation of image
     *      - on index 0: rows
     *      - on index 1: cols
     */
    private int[][] summation;
    private int sumRowMax;
    private int sumColMax;
    /**
     * Because the image is always square, size = width = height
     */
    private int size;
    /**
     * Angle of projection in which was image array created
     */
    private int angle;

    public ImageData(int[][] imageArray, int[][] summation, Integer angle) throws ImageDataException
    {
        if (imageArray == null || imageArray.length == 0)
        {
            throw new ImageDataException("Pole obrázku nesmí být prázdné.");
        }

        if (summation == null || summation.length == 0)
        {
            throw new ImageDataException("Pole sumace nesmí být prázdné.");
        }

        if (angle == null)
        {
            throw new ImageDataException("Úhel nesmí být prázdný.");
        }

        int max = -1;
        for (int i = 0; i < summation[0].length; i++){
            if(summation[0][i] > max){
                max = summation[0][i];
            }
        }
        this.sumRowMax = max;

        for (int i = 0; i < summation[1].length; i++){
            if(summation[1][i] > max){
                max = summation[1][i];
            }
        }
        this.sumColMax = max;

        this.imageArray = imageArray;
        this.summation = summation;
        this.size = imageArray.length;
        this.angle = angle;
    }

    /**
     * Returns image array
     *
     * @return int[][]
     */
    public int[][] getImageArray()
    {
        return imageArray;
    }

    /**
     * Returns array with summation array
     *
     * @return int[][]
     */
    public int[][] getSummation()
    {
        return summation;
    }

    /**
     * Setter for image array
     *
     * @param imageArray int[][]
     */
    public void setImageArray(int[][] imageArray)
    {
        this.imageArray = imageArray;
    }

    /**
     * Returns size of the image.
     * size = width = height
     *
     * @return int
     */
    public int getSize()
    {
        return size;
    }

    /**
     * Setter for size of the image
     *
     * @param size int
     */
    public void setSize(int size)
    {
        this.size = size;
    }

    /**
     * Returns angle of the projection
     *
     * @return int
     */
    public int getAngle()
    {
        return angle;
    }

    /**
     * Setter for angle of the projection
     *
     * @param angle int
     */
    public void setAngle(int angle)
    {
        this.angle = angle;
    }

    public BufferedImage createImage()
    {
        IImageProcessor processor = new ImageProcessor();
        return processor.createImageFromArray(this.imageArray);
    }

    public int getSumRowMax()
    {
        return sumRowMax;
    }

    public void setSumRowMax(int sumRowMax)
    {
        this.sumRowMax = sumRowMax;
    }

    public int getSumColMax()
    {
        return sumColMax;
    }

    public void setSumColMax(int sumColMax)
    {
        this.sumColMax = sumColMax;
    }
}
