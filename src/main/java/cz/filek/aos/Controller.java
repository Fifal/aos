package cz.filek.aos;

import cz.filek.aos.entity.ImageDataCollection;
import cz.filek.aos.image.IImageProcessor;
import cz.filek.aos.image.ImageProcessor;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

public class Controller implements Initializable
{
    @FXML
    private BorderPane borderPane;

    @FXML
    private ImageView imageView;

    @FXML
    private ImageView imgResult;

    @FXML
    private TextArea loggerArea;

    @FXML
    private TextField txtProjectionsCount;

    @FXML
    private ChoiceBox selectMethod;

    @FXML
    private Button btnStop;

    @FXML
    private Button btnCompute;

    @FXML
    private Button btnSinogram;

    @FXML
    private Button btnFilteredSinogram;

    @FXML
    private Button btnLoadImage;

    @FXML
    private Button btnSave;

    @FXML
    private ProgressBar progressBar;

    private IImageProcessor imageProcessor;

    private BufferedImage loadedImage;

    private Thread thread;

    private String originalFileName;

    enum ECalc
    {
        SIMPLE,
        SIMPLE_FILTERED,
        MULTIPLICATIVE,
        SINOGRAM,
        SINOGRAM_FILTERED
    }

    public void initialize(URL location, ResourceBundle resources)
    {
        imageProcessor = new ImageProcessor();
        allowStart(true);
    }

    /**
     * Shows File chooser dialog
     */
    public void loadImage()
    {
        this.clearLog();

        String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Obrázky", "*.jpg", "*.jpeg", "*.png", "*.bmp", "*.gif");

        FileChooser dialog = new FileChooser();

        File images = new File(currentPath + "/images");
        File current = new File(currentPath);

        dialog.setInitialDirectory(images.exists() ? images : current);
        dialog.setTitle("Vyberte obrázek");
        dialog.getExtensionFilters().add(extensionFilter);

        File file = dialog.showOpenDialog(borderPane.getScene().getWindow());

        if (file == null)
        {
            return;
        }

        logInfo("Načítám obrázek");
        if (!imageProcessor.checkImageIsValid(file))
        {
            logError("Obrázek se nepodařilo načíst nebo se nejedná o čtvercový obrázek.");
            return;
        }
        logInfo("Převádím obrázek do šedotónu.");
        BufferedImage image = imageProcessor.convertToGreyscale(file);

        setImagePreview(SwingFXUtils.toFXImage(image, null));

        this.loadedImage = image;
        this.originalFileName = file.getName();
    }

    /**
     * Calls method based on choicebox
     */
    public void calculate()
    {
        if (isNotImageLoaded())
        {
            return;
        }

        if (isProjectionCountNotOk())
        {
            return;
        }

        switch ((String) selectMethod.getValue())
        {
            case "Prostá sumační":
            {
                startCalculation(ECalc.SIMPLE);
                break;
            }
            case "Sumační s filtrováním":
            {
                startCalculation(ECalc.SIMPLE_FILTERED);
                break;
            }
            case "Multiplikativní":
            {
                startCalculation(ECalc.MULTIPLICATIVE);
                break;
            }
        }
    }

    /**
     * Shows sinogram of given image
     */
    public void showSinogram()
    {
        if (isNotImageLoaded())
        {
            return;
        }

        startCalculation(ECalc.SINOGRAM);
    }

    /**
     * Shows filtered sinogram of loaded image
     */
    public void showFilteredSinogram()
    {
        if (isNotImageLoaded())
        {
            return;
        }

        startCalculation(ECalc.SINOGRAM_FILTERED);
    }

    /**
     * Checks if image wasn't loaded and returns true if wasn't
     *
     * @return boolean
     */
    private boolean isNotImageLoaded()
    {
        if (this.loadedImage == null)
        {
            showAlert("Chyba", "Chyba při počítání rekonstrukce", "Nejprve musíte vybrat obrázek, pro který se má rekonstrukce spočítat.", Alert.AlertType.ERROR);
            return true;
        }

        return false;
    }

    /**
     * Checks if projection count is not whole number divisible by 2
     *
     * @return true if it's not, false otherwise
     */
    private boolean isProjectionCountNotOk()
    {
        try
        {
            int projectionCount = Integer.parseInt(txtProjectionsCount.getText());
            if (projectionCount % 2 != 0 || projectionCount < 2 || projectionCount > 360)
            {
                showAlert("Chyba", "Chyba při počítání rekonstrukce", "Počet projekcí musí být celé sudé číslo v rozsahu 2–360.", Alert.AlertType.WARNING);
                return true;
            }
        } catch (Exception ex)
        {
            showAlert("Chyba", "Chyba při počítání rekonstrukce", "Počet projekcí musí být celé sudé číslo v rozsahu 2–360.", Alert.AlertType.ERROR);
            return true;
        }

        return false;
    }

    /**
     * Sets image preview with loaded file converted to greyscale
     *
     * @param image Image
     */
    private void setImagePreview(final Image image)
    {
        Platform.runLater(() -> imageView.setImage(image));
    }

    /**
     * Sets image result
     *
     * @param image Image
     */
    private void setImageResult(final Image image)
    {
        Platform.runLater(() -> imgResult.setImage(image));
    }

    /**
     * Logs info to text area logger
     *
     * @param message String
     */
    private void logInfo(final String message)
    {
        log("INFO. " + message);
    }

    /**
     * Logs error to text area logger
     *
     * @param message String
     */
    private void logError(final String message)
    {
        log("CHYBA! " + message);
    }

    /**
     * Adds text to logging text area with current time
     *
     * @param message String
     */
    private void log(final String message)
    {
        final DateFormat dateFormat = new SimpleDateFormat("[HH:mm:ss]: ");
        final Date date = new Date();

        Platform.runLater(() -> loggerArea.appendText(dateFormat.format(date) + message + "\n"));
    }

    /**
     * Clears text in text area logger
     */
    private void clearLog()
    {
        Platform.runLater(() -> loggerArea.setText(""));
    }

    /**
     * Function shows alert with error message
     *
     * @param title   String title of the alert dialog
     * @param header  String header
     * @param message String content of the error
     * @param type    AlertType type of the alert
     */
    private void showAlert(String title, String header, String message, Alert.AlertType type)
    {
        if (type == Alert.AlertType.ERROR)
        {
            logError(message);
        }

        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);

        alert.showAndWait();
    }

    /**
     * Starts calculation thread with selected method
     *
     * @param calc : Type of method
     */
    private void startCalculation(ECalc calc)
    {
        Runnable task = null;

        switch (calc)
        {
            case SINOGRAM:
            {
                task = () -> {
                    logInfo("Počítám sinogram pro obrázek.");
                    ImageDataCollection imageDataCollection = this.imageProcessor.getImageDataCollection(this.loadedImage, 360);
                    setImageResult(SwingFXUtils.toFXImage(imageProcessor.createSinogram(imageDataCollection), null));
                    logInfo("Sinogram byl vytvořen.");
                    allowStart(true);
                };
                break;
            }
            case SINOGRAM_FILTERED:
            {
                task = () -> {
                    logInfo("Počítám filtrovaný sinogram pro obrázek.");
                    ImageDataCollection imageDataCollection = this.imageProcessor.getImageDataCollection(this.loadedImage, 360);
                    setImageResult(SwingFXUtils.toFXImage(imageProcessor.createFilteredSinogram(imageDataCollection), null));
                    logInfo("Sinogram byl vytvořen.");
                    allowStart(true);
                };
                break;
            }
            case SIMPLE:
            {
                task = () -> {
                    logInfo("Počátám rekonstrukci jednoduchou sumační metodou.");
                    ImageDataCollection collection = calculateProjections();
                    if (collection == null)
                    {
                        logError("Nepodařilo se spočítat projekce.");
                        return;
                    }
                    int[][] result = this.imageProcessor.getSimpleSummation(collection);
                    setImageResult(SwingFXUtils.toFXImage(this.imageProcessor.createImageFromArray(result), null));
                    logInfo("Rekonstrukce dokončena.");
                    allowStart(true);
                };
                break;
            }
            case SIMPLE_FILTERED:
                // FIR, Gauss, Ram-lack, shep-logen, hamming
                task = () -> {
                    logInfo("Počátám rekonstrukci filtrovanou sumační metodou.");
                    ImageDataCollection collection = calculateProjections();
                    if (collection == null)
                    {
                        logError("Nepodařilo se spočítat projekce.");
                        return;
                    }
                    int[][] result = this.imageProcessor.getSimpleFilteredSummation(this.loadedImage, collection);
                    setImageResult(SwingFXUtils.toFXImage(this.imageProcessor.createImageFromArray(result), null));
                    logInfo("Rekonstrukce dokončena.");
                    allowStart(true);
                };
                break;
            case MULTIPLICATIVE:
            {
                task = () -> {
                    logInfo("Počítám rekonstrukci multiplikativní metodou.");
                    ImageDataCollection collection = calculateProjections();
                    if (collection == null)
                    {
                        logError("Nepodařilo se spočítat projekce.");
                        return;
                    }
                    int[][] result = this.imageProcessor.getMultiplicative(collection);
                    setImageResult(SwingFXUtils.toFXImage(this.imageProcessor.createImageFromArray(result), null));
                    logInfo("Rekonstrukce dokončena.");
                    allowStart(true);
                };
                break;
            }
        }
        Thread thread = new Thread(task);
        this.thread = thread;
        thread.start();
        allowStart(false);
    }

    /**
     * Stops the calculation thread
     */
    public void stopCalculation()
    {
        this.thread.stop();
        logInfo("Výpočet byl zrušen.");
        allowStart(true);
    }

    /**
     * Sets button states
     *
     * @param isAllowed is new calculation start allowed
     */
    private void allowStart(boolean isAllowed)
    {
        Platform.runLater(() -> {
            if (isAllowed)
            {
                btnCompute.setDisable(false);
                btnFilteredSinogram.setDisable(false);
                btnSinogram.setDisable(false);
                btnLoadImage.setDisable(false);
                btnSave.setDisable(false);
                btnStop.setDisable(true);
                progressBar.setProgress(0);


            } else
            {
                btnCompute.setDisable(true);
                btnFilteredSinogram.setDisable(true);
                btnSinogram.setDisable(true);
                btnLoadImage.setDisable(true);
                btnSave.setDisable(true);
                btnStop.setDisable(false);
                progressBar.setProgress(ProgressBar.INDETERMINATE_PROGRESS);
            }
        });
    }

    /**
     * Calculates projection collection
     *
     * @return ImageDataCollection
     */
    private ImageDataCollection calculateProjections()
    {
        ImageDataCollection collection;

        try
        {
            int projectionCount = Integer.parseInt(txtProjectionsCount.getText());
            logInfo("Počítám projekce obrázku, počet projekcí: " + projectionCount + ".");
            collection = this.imageProcessor.getImageDataCollection(this.loadedImage, projectionCount);
            logInfo("Projekce spočítány.");
        } catch (Exception ex)
        {
            return null;
        }

        return collection;
    }

    /**
     * Saves result into file
     */
    public void saveResult()
    {
        Image image = this.imgResult.getImage();

        if (image == null)
        {
            return;
        }

        FileChooser fileChooser = new FileChooser();
        String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();

        File results = new File(currentPath + "/results");
        File current = new File(currentPath);

        //Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG", "*.png");
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setInitialFileName("result-" + this.originalFileName);
        fileChooser.setInitialDirectory(results.exists() ? results : current);


        //Show save file dialog
        File file = fileChooser.showSaveDialog(borderPane.getScene().getWindow());

        if (file != null)
        {
            try
            {
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
