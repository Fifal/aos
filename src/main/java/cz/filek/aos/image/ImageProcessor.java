package cz.filek.aos.image;

import cz.filek.aos.entity.ImageData;
import cz.filek.aos.entity.ImageDataCollection;
import cz.filek.aos.exceptions.ImageDataException;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class ImageProcessor implements IImageProcessor
{
    private enum ESummationType
    {
        COLUMN,
        ROW
    }

    /**
     * Loads image from file into 2D array
     *
     * @param image : Image file
     * @return null|int[][]
     */
    @Override
    public int[][] loadImageToArray(BufferedImage image)
    {
        // Check if file is not null
        if (image == null)
        {
            return null;
        }

        int width = image.getWidth();
        int height = image.getHeight();

        int[][] array = new int[width][height];
        for (int y = 0; y < image.getHeight(); y++)
        {
            for (int x = 0; x < image.getWidth(); x++)
            {
                array[x][y] = getColorValue(image.getRGB(x, y));
            }
        }

        return array;
    }

    /**
     * Retruns Image converted to greyscale
     *
     * @param file : Image file
     * @return null|BufferedImage
     */
    @Override
    public BufferedImage convertToGreyscale(File file)
    {
        // Try to load an Image
        BufferedImage image = null;
        try
        {
            image = ImageIO.read(file);
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        // If image was not loaded, return null
        if (image == null)
        {
            return null;
        }

        int width = image.getWidth();
        int height = image.getHeight();

        // Convert to greyscale
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int p = image.getRGB(x, y);

                int a = (p >> 24) & 0xff;
                int r = (p >> 16) & 0xff;
                int g = (p >> 8) & 0xff;
                int b = p & 0xff;

                // Calculate average
                int avg = (int) (0.299 * r + 0.587 * g + 0.114 * b);

                // Replace RGB value with avg
                int gray = (a << 24) | (avg << 16) | (avg << 8) | avg;

                image.setRGB(x, y, gray);
            }
        }

        int[] maxSize = getMaxSize(image);

        BufferedImage result = new BufferedImage(maxSize[0], maxSize[1], image.getType());
        Graphics2D graphics2D = result.createGraphics();
        graphics2D.drawImage(image, (maxSize[0] / 2) - (width / 2), (maxSize[1] / 2) - (height / 2), null);
        graphics2D.dispose();

        return result;
    }

    /**
     * Returns summation of the image array – simple method
     *
     * @param collection : ImageDataCollection
     * @return int[][]
     */
    @Override
    public int[][] getSimpleSummation(ImageDataCollection collection)
    {
        int size = collection.getImageSize();
        int[][] result = new int[size][size];
        ArrayList<BufferedImage> images = new ArrayList<>();

        int c = 0;
        for (ImageData imageData : collection.getImages())
        {
            for (int i = 0; i < imageData.getSummation()[1].length; i++)
            {
                for (int y = 0; y < size; y++)
                {
                    result[y][i] = imageData.getSummation()[1][i];
                }
            }

            BufferedImage image = this.createImageFromArray(result);
            images.add(this.rotateImage(image, c * collection.getAngle()));
            c++;
        }

        return getSummationMatrix(images, size);
    }

    /**
     * Returns simple summation method with filtration
     *
     * @param loadedImage BufferedImage
     * @param collection  ImageDataCollection
     * @return int[][]
     */
    @Override
    public int[][] getSimpleFilteredSummation(BufferedImage loadedImage, ImageDataCollection collection)
    {

        ImageDataCollection imageDataCollection = getImageDataCollection(loadedImage, 360);
        BufferedImage sinogram = this.createFilteredSinogram(imageDataCollection);

        int[][] filtered = this.loadImageToArray(sinogram);
        int colSize = collection.getSize();
        int size = collection.getImageSize();
        int[][] result = new int[size][size];
        ArrayList<BufferedImage> images = new ArrayList<>();

        for (int c = 0; c < colSize; c++)
        {
            for (int i = 0; i < size; i++)
            {
                int row = c * collection.getAngle();
                System.arraycopy(filtered[row], 0, result[i], 0, size);
            }

            BufferedImage image = this.createImageFromArray(result);
            images.add(this.rotateImage(image, c * collection.getAngle()));
        }

        return getSummationMatrix(images, size);
    }

    /**
     * Returns summation matrix for given images and size of images
     * @param images : ArrayList<BufferedImage>
     * @param size : int
     * @return int[][] : summation matrix
     */
    private int[][] getSummationMatrix(ArrayList<BufferedImage> images, int size)
    {
        int[][] result = new int[size][size];
        for (BufferedImage image : images)
        {
            int[][] matrix = this.loadImageToArray(image);
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    result[j][i] += matrix[i][j];
                }
            }
        }

        return result;
    }

    /**
     * Returns multiplicative method result
     *
     * @param collection : ImageDataCollection
     * @return int[][]
     */
    @Override
    public int[][] getMultiplicative(ImageDataCollection collection)
    {
        int size = collection.getImageSize();
        BigDecimal[][] summationMatrix = new BigDecimal[size][size];
        ArrayList<BufferedImage> images = new ArrayList<>();

        int c = 0;
        for (ImageData imageData : collection.getImages())
        {
            for (int i = 0; i < imageData.getSummation()[1].length; i++)
            {
                for (int y = 0; y < size; y++)
                {
                    summationMatrix[y][i] = BigDecimal.valueOf(imageData.getSummation()[1][i]);
                }
            }

            BufferedImage image = this.createImageFromArray(summationMatrix);
            images.add(this.rotateImage(image, c * collection.getAngle()));
            c++;
        }

        BigDecimal[][] bigValues = new BigDecimal[size][size];
        BigDecimal bigValuesMax = BigDecimal.valueOf(-1);

        c = 0;
        for (BufferedImage image : images)
        {
            int[][] matrix = this.loadImageToArray(image);
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    int value = (matrix[j][i]);

                    if (c == 0)
                    {
                        bigValues[j][i] = BigDecimal.valueOf(value);
                    } else
                    {
                        bigValues[j][i] = bigValues[j][i].multiply(BigDecimal.valueOf(value));
                    }

                    if (bigValues[j][i].compareTo(bigValuesMax) > 0)
                    {
                        bigValuesMax = bigValues[j][i];
                    }
                }
            }
            c++;
        }

        double[][] temp = new double[size][size];
        double doubleMax = -1;
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                double value = (bigValues[j][i].divide(bigValuesMax, 128, RoundingMode.HALF_UP).multiply(new BigDecimal(255))).doubleValue();
                double vv = Math.pow(value, 1.0 / (collection.getImages().size() / 3.75));
                temp[i][j] = vv;

                if (vv > doubleMax)
                {
                    doubleMax = vv;
                }
            }
        }

        int[][] result = new int[size][size];
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                double value = (temp[i][j] / doubleMax) * 255;
                result[i][j] = (int) value;
            }
        }

        return result;
    }

    /**
     * Checks if File is image and that image has the same width as height
     *
     * @param file : File with image
     * @return true if image is valid, false otherwise
     */
    @Override
    public boolean checkImageIsValid(File file)
    {
        BufferedImage bufferedImage;
        try
        {
            bufferedImage = ImageIO.read(file);
        } catch (IOException e)
        {
            return false;
        }

        return bufferedImage.getWidth() == bufferedImage.getHeight();
    }

    /**
     * Rotates image by given angle (0 - 360°)
     *
     * @param image : BufferedImage to rotate
     * @param angle : int angle <0-360>
     * @return BufferedImage
     */
    @Override
    public BufferedImage rotateImage(BufferedImage image, int angle)
    {
        double rads = Math.toRadians(angle);

        BufferedImage rotated = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        Graphics2D g2d = rotated.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        AffineTransform at = new AffineTransform();

        int x = image.getWidth() / 2;
        int y = image.getHeight() / 2;

        at.rotate(rads, x, y);
        g2d.setTransform(at);
        g2d.drawImage(image, 0, 0, null);
        g2d.dispose();

        return rotated;
    }

    /**
     * Returns collection with images data for given number of projections
     *
     * @param image            BufferedImage
     * @param projectionsCount int
     * @return ImageDataCollection
     */
    @Override
    public ImageDataCollection getImageDataCollection(BufferedImage image, int projectionsCount)
    {
        int increment = projectionsCount == 2 ? 90 : (360 / projectionsCount);

        ImageDataCollection collection = new ImageDataCollection(increment);
        for (int i = 0; i < projectionsCount; i++)
        {
            int angle = i * increment;
            BufferedImage rotated = this.rotateImage(image, angle);
            int[][] imageArray = this.loadImageToArray(rotated);

            int[][] summation = new int[2][];
            summation[0] = this.getSummation(ESummationType.ROW, imageArray);
            summation[1] = this.getSummation(ESummationType.COLUMN, imageArray);

            try
            {
                ImageData imageData = new ImageData(imageArray, summation, angle);
                collection.addImageData(imageData);
            } catch (ImageDataException e)
            {

            }
        }

        return collection;
    }

    /**
     * Creates BufferedImage from image array
     *
     * @param imageArray int[][]
     * @return BufferedImage
     */
    @Override
    public BufferedImage createImageFromArray(int[][] imageArray)
    {
        int width = imageArray.length;
        int height = imageArray[0].length;

        int maxValue = -1;
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (imageArray[i][j] > maxValue) maxValue = imageArray[i][j];
            }
        }

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int value = (int) (((double) imageArray[x][y] / (double) maxValue) * 255);
                Color color = new Color(value, value, value);
                image.setRGB(x, y, color.getRGB());
            }
        }

        return image;
    }

    /**
     * Creates image from array of BigDecimals
     *
     * @param imageArray BigDecimal[][] array
     * @return BufferedImage
     */
    private BufferedImage createImageFromArray(BigDecimal[][] imageArray)
    {
        int width = imageArray.length;
        int height = imageArray[0].length;

        BigDecimal maxValue = BigDecimal.valueOf(-1);
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (imageArray[i][j].compareTo(maxValue) > 0) maxValue = imageArray[i][j];
            }
        }

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int value;
                if (imageArray[x][y].compareTo(BigDecimal.valueOf(255)) > 0)
                    value = ((imageArray[x][y].divide(maxValue, 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(255)).intValue()));
                else
                    value = imageArray[x][y].intValue();

                Color color = new Color(value, value, value);
                image.setRGB(x, y, color.getRGB());

            }
        }

        return image;
    }

    /**
     * Creates sinogram from projections
     *
     * @param collection ImageDataCollection
     * @return BufferedImage
     */
    @Override
    public BufferedImage createSinogram(ImageDataCollection collection)
    {
        ArrayList<ImageData> images = collection.getImages();
        int height = images.get(0).getSize();
        BufferedImage image = new BufferedImage(images.size(), height, BufferedImage.TYPE_BYTE_GRAY);

        int i = 0;
        for (ImageData imageData : images)
        {
            for (int y = 0; y < imageData.getSummation()[1].length; y++)
            {
                int value = (int) (((double) imageData.getSummation()[1][y] / (double) collection.getMaxValue().intValue()) * 255);
                Color color = new Color(value, value, value);
                image.setRGB(i, y, color.getRGB());
            }
            i++;
        }

        return image;
    }

    /**
     * Creates filtered sinogram
     *
     * @param collection ImageDataCollection
     * @return BufferedImage
     */
    @Override
    public BufferedImage createFilteredSinogram(ImageDataCollection collection)
    {
        float[] kernel = {
                -1, -1, -1,
                -1, 9.25f, -1,
                -1, -1, -1
        };

        BufferedImage sinogram = this.createSinogram(collection);
        BufferedImage temp = new BufferedImage(sinogram.getWidth(), sinogram.getHeight(), sinogram.getType());

        BufferedImageOp op = new ConvolveOp(new Kernel(3, 3, kernel));
        op.filter(sinogram, temp);

        return temp;
    }

    /**
     * Returns value of the color in the range of 0-255
     *
     * @param colorValue : Color value
     * @return int
     */
    private int getColorValue(int colorValue)
    {
        Color color = new Color(colorValue);
        int r = color.getRed();
        int g = color.getGreen();
        int b = color.getBlue();

        return (r + g + b) / 3;
    }

    /**
     * Returns simple summation of array by type – row or column
     *
     * @param summationType : ESummationType
     * @param imageArray    : Array with image
     * @return int[] simple summation
     */
    private int[] getSummation(ESummationType summationType, int[][] imageArray)
    {
        int width = imageArray.length;
        int height = imageArray[0].length;

        int[] sumArray = new int[width];
        for (int y = 0; y < height; y++)
        {
            int rowSum = 0;
            for (int x = 0; x < width; x++)
            {
                if (summationType == ESummationType.ROW)
                {
                    rowSum += imageArray[x][y];
                } else
                {
                    rowSum += imageArray[y][x];
                }
            }
            sumArray[y] = rowSum;
        }
        return sumArray;
    }

    /**
     * Returns max size which image could be when rotating
     *
     * @param image : Image
     * @return int[]
     */
    private int[] getMaxSize(BufferedImage image)
    {
        double rads = Math.toRadians(45);
        double sin = Math.abs(Math.sin(rads)), cos = Math.abs(Math.cos(rads));
        int w = image.getWidth();
        int h = image.getHeight();
        int newWidth = (int) Math.floor(w * cos + h * sin);
        int newHeight = (int) Math.floor(h * cos + w * sin);

        return new int[]{newWidth, newHeight};
    }
}
