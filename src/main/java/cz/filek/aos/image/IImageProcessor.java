package cz.filek.aos.image;

import cz.filek.aos.entity.ImageDataCollection;

import java.awt.image.BufferedImage;
import java.io.File;

public interface IImageProcessor
{
    /**
     * Loads image from file into 2D array
     * @param image : Image file
     * @return null|int[][]
     */
    int[][] loadImageToArray(BufferedImage image);

    /**
     * Returns Image converted to greyscale and resize it to maximum size when rotating
     *
     * @param file : Image file
     * @return null|BufferedImage
     */
    BufferedImage convertToGreyscale(File file);

    /**
     * Returns summation of the image array – simple method
     *
     * @param collection : ImageDataCollection
     * @return int[][]
     */
    int[][] getSimpleSummation(ImageDataCollection collection);

    /**
     * Returns simple summation method with filtration
     *
     * @param loadedImage BufferedImage
     * @param collection ImageDataCollection
     * @return int[][]
     */
    int[][] getSimpleFilteredSummation(BufferedImage loadedImage, ImageDataCollection collection);

    /**
     * Returns multiplicative method result
     *
     * @param collection : ImageDataCollection
     * @return int[][]
     */
    int[][] getMultiplicative(ImageDataCollection collection);

    /**
     * Checks if File is image and that image has the same width as height
     *
     * @param file : File with image
     * @return true if image is valid, false otherwise
     */
    boolean checkImageIsValid(File file);

    /**
     * Rotates image by given angle (0 - 360°)
     *
     * @param image : BufferedImage to rotate
     * @param angle : int angle <0-360>
     * @return BufferedImage
     */
    BufferedImage rotateImage(BufferedImage image, int angle);

    /**
     * Returns collection with images data for given number of projections
     *
     * @param image BufferedImage
     * @param projectionsCount int
     * @return ImageDataCollection
     */
    ImageDataCollection getImageDataCollection(BufferedImage image, int projectionsCount);

    /**
     * Creates BufferedImage from image array
     *
     * @param imageArray int[][]
     * @return BufferedImage
     */
    BufferedImage createImageFromArray(int[][] imageArray);

    /**
     * Creates sinogram from projections
     *
     * @param collection ImageDataCollection
     * @return BufferedImage
     */
    BufferedImage createSinogram(ImageDataCollection collection);

    /**
     * Creates filtered sinogram
     *
     * @param collection ImageDataCollection
     * @return BufferedImage
     */
    BufferedImage createFilteredSinogram(ImageDataCollection collection);
}
